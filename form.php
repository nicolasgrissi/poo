
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<?php

// Autoloader - BEGIN
require './classes/Autoloader.class.php';
Autoloader::Register();

$form1 = new Form(array(
    "nom" => "Durand",
    "prenom" => "Michel"
));
$Form2 = new FormBootstrap(array(
    "nom" => "Durand"
));

$Personnage = new Personnage("Merlin", Personnage::FORCE_MOYENNE);


echo($form1->Input("text", "Nom", ($form1->data["nom"])) );
echo($form1->Input("text", "Prénom", ($form1->data["prenom"])) );
echo $form1->Submit();

$Personnage::Crier();

echo $form1->tag_surround;
echo "<br>";

$compt1 = new Compteur();
echo "La classe \"Compteur\" à été instanciée ".Compteur::getCompteur()." fois <br>";

$compt2 = new Compteur();
echo "La classe \"Compteur\" à été instanciée ".Compteur::getCompteur()." fois <br>";

$compt3 = new Compteur();
echo "La classe \"Compteur\" à été instanciée ".Compteur::getCompteur()." fois <br>";

$compt4 = new Compteur();
echo "La classe \"Compteur\" à été instanciée ".Compteur::getCompteur()." fois <br>";

$compt5 = new Compteur();
echo "La classe \"Compteur\" à été instanciée ".Compteur::getCompteur()." fois <br>";
echo "La classe \"Compteur\" à été instanciée ".Compteur::getCompteur()." fois <br>";
echo "La classe \"Compteur\" à été instanciée ".Compteur::getCompteur()." fois <br>";
echo "La classe \"Compteur\" à été instanciée ".Compteur::getCompteur()." fois <br>";

echo "dans l'objet aussi la valeur du Compteur vaut " . $compt5->getCompteur();



?>
</body>
</html>





