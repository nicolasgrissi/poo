<?php

namespace TP;

class Personnages
{
    private $_id;
    private $_nom;
    private $_forcePerso;
    private $_degats;
    private $_niveau;
    private $_experience;

    public function __construct(array $donnees)
    {
        $this->hydrate($donnees);
    }

    public function hydrate(array $donnees)
    {
        foreach ($donnees as $key => $value) {
            // on recupère le nom du setter correspondant à l'attribut
            $method = 'set' . ucfirst($key);
            // si le setter correspondant existe
            if (method_exists($this, $method))
            {
                // on appelle le setter
                $this->$method($value);
            }
        }
       /* if (isset($donnees['id'])) {
            $this->setId($donnees['id']);
        }
        if (isset($donnees['nom'])) {
            $this->setNom($donnees['nom']);
        }
        if (isset($donnees['forcePerso'])) {
            $this->setForcePerso($donnees['forcePerso']);
        }
        if (isset($donnees['degats'])) {
            $this->setDegats($donnees['degats']);
        }
        if (isset($donnees['niveau'])) {
            $this->setNiveau($donnees['niveau']);
        }
        if (isset($donnees['experience'])) {
            $this->setExperience($donnees['experience']);
        }*/
    }

    //liste des getters

    public function getId() { return $this->_id; }

    public function getNom() { return $this->_nom; }

    public function getForcePerso() { return $this->_forcePerso; }

    public function getDegats() { return $this->_degats; }

    public function getNiveau() { return $this->_niveau; }

    public function getExperience() { return $this->_experience; }

    // liste des setters
    public function setId($id)
    {
        $id = (int)$id;
        if ($id > 0) {
            $this->_id = $id;
        }
    }

    public function setNom($nom)
    {
        if (is_string($nom)) {
            $this->_nom = $nom;
        }
    }

    public function setForcePerso($forcePerso)
    {
        $forcePerso = (int)$forcePerso;
        if ($forcePerso >= 1 && $forcePerso <= 120) {
            $this->_forcePerso = $forcePerso;
        }
    }

    public function setDegats($degats)
    {
        $degats = (int)$degats;
        if ($degats >= 1 && $degats <= 120) {
            $this->_degats = $degats;
        }
    }

    public function setNiveau($niveau)
    {
        $niveau = (int)$niveau;
        if ($niveau >= 1 && $niveau <= 100) {
            $this->_niveau = $niveau;
        }
    }

    public function setExperience($experience)
    {
        if ($experience >= 1 && $experience <= 100) {
            $this->_experience = $experience;
        }
    }


}