<?php

class Personnage
{

    protected $vie = 80;
    protected $atk = 20;
    private $nom;
    private $_force;

    const FORCE_PETITE = 20;
    const FORCE_MOYENNE = 50;
    const FORCE_GRANDE = 80;

    public function __construct($nom , $force)
    {
        $this->nom = $nom;
        $this->_force = $force;
    }

    public function setForce($force)
    {
    if(in_array($force , [self::FORCE_PETITE , self::FORCE_MOYENNE , self::FORCE_GRANDE ])){
        $this->_force = $force;
        }
    }




    public static function Crier()
    {
        echo "HOLA AMIGO !!";
    }

    public function Regenerer($vie = null)
    {
        if (is_null($vie)) {
            $this->vie = 100;
        } else {
            $this->vie += $vie;
        }
    }

    public function Mort()
    {
        if ($this->vie <= 0) {
            return true;
        } else {
            return false;
        }
    }

    public function Attaque($cible)
    {
        $cible->vie -= $this->atk;
    }

    public function GetVie()
    {
        return $this->vie;
    }

    public function SetNom($nom)
    {
        return $this->nom = $nom;
    }

    public function GetNom()
    {
        return $this->nom;
    }
}