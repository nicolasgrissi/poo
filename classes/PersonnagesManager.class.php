<?php

namespace TP;

class PersonnagesManager
{

    private $_db;

    public function __construct($db)
    {
        $this->setDb($db);
    }


    public function add($perso)
    {
        //Action de type add
        $q = $this->_db->prepare('INSERT INTO personnages (nom, forcePerso, degats, niveau, experience) VALUES (:nom, :forcePerso, :degats,:niveau, :experience )');
        $q->bindValue(':nom', $perso->getNom());
        $q->bindValue(':forcePerso', $perso->getForcePerso(),\PDO::PARAM_INT);
        $q->bindValue(':degats', $perso->getDegats());
        $q->bindValue(':niveau', $perso->getNiveau(),\PDO::PARAM_INT);
        $q->bindValue(':experience', $perso->getExperience(),\PDO::PARAM_INT);

        $q->execute();
    }

    public function delete($perso)
    {
        // Action de type delete
        $this->_db->exec('DELETE FROM personnages WHERE id=' . $perso->id());
    }

    public function get($id)
    {
        // execute  une requete de type SELECT avec un WHERE id et retourne un objet personnage
        $id = (int)$id;

        $q = $this->_db->query('SELECT id , nom , forcePerso , degats , niveau , experience FROM personnages');
        $donnees = $q->fetch();
        return new Personnages($donnees);
    }

    public function getList()
    {
        $persos = [];
        $q = $this->_db->query('SELECT id , nom , forcePerso , degats , experience FROM personnages ORDER BY nom');
        while ($donnees = $q->fetch(\PDO::FETCH_ASSOC))
        {
            $persos[] = new Personnages($donnees);
        }
        return $persos;
    }

    public function update($perso)
    {
        // execute une acton de type update
        $q = $this->_db->prepare('UPDATE personnages SET forcePerso = :forcePerso , degats = :degats , niveau = :niveau , experience = :experience WHERE id = :id');

        $q->bindValue(':forcePerso', $perso->forcePerso());
        $q->bindValue(':degats', $perso->degats());
        $q->bindValue(':niveau', $perso->niveau());
        $q->bindValue(':experience', $perso->experience());
        $q->bindValue(':id', $perso->id());

        $q->execute();
    }

    /**
     * @param mixed $db
     */
    public function setDb($db)
    {
        $this->_db = $db;
    }
}