<?php

/**
 * Class Autoloader
 * Charge automatiquement toutes les classes
 */
class Autoloader
{

    /**
     * function Register
     * Autoload class
     */
    static function Register()
    {
        spl_autoload_register(array('Autoloader', 'Autoload'));
    }

    static function Autoload($class_name)
    {
        $class_name = str_replace("TP\\", '',$class_name);



        //$class_name = str_replace("\\", '/', $class_name);

        require 'classes\\' . $class_name . '.class.php';
    }
}