<?php


/**
 * Class Form
 */
class Form
{

    public $data = array();
    public $tag_surround = 'p';

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function Input($type = 'text', $label = null, $value = null)
    {
        return $this->Surround("<label>{$label}</label><br><input type='{$type}' name='{$label}' value='{$value}'>");
    }

    public function Submit()
    {
        return $this->Surround("<div class='form-group'>
            <div class='col-sm-offset-2 col-sm-10'>
                <button type='submit' class='btn btn-default'>Send</button>
            </div>
        </div>
        
        ");
    }


    protected function Surround($html)
    {
        return "<div class='container'><{$this->tag_surround}>{$html}</{$this->tag_surround}></div>";
    }
}
