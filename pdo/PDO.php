<?php
// Config DSN
DEFINE('DB_HOST','localhost');
DEFINE('DB_NAME','test');

//Config mysql keys
DEFINE('DB_USER','root');
DEFINE('DB_PASSWORD','');



try {
	$db = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8', DB_USER, DB_PASSWORD,
        array
	(PDO::ATTR_ERRMODE
	 => PDO::ERRMODE_EXCEPTION)); // tester la connexion à la bdd
} catch (Exception $e) {
	die('Erreur : ' . $e->getMessage()); // si ratée affichée la connexion à la bdd
}